require_dependency "twilio_surveyor_stats/application_controller"

module TwilioSurveyor
  class DocStatsController < TwilioSurveyor::DocsController
    before_action :authenticate_admin!
    before_action :set_stats_for_organization, only: [:show, :index]

    def index
      super
      render template: 'twilio_surveyor/docs/index'
    end

    def show
      super
      render template: 'twilio_surveyor/docs/show'
    end

  private

    def set_stats_for_organization
      @stats_for_organization_id = params[:stats_for_organization_id]
    end
  end
end
