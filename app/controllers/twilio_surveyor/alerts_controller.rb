require_dependency "twilio_surveyor_stats/application_controller"

module TwilioSurveyor
  class AlertsController < ApplicationController
    before_action :authenticate_admin!
    before_action :set_alert, only: [:show, :edit, :update, :destroy]

    # GET /alerts
    def index
      @alerts = Alert.all
    end

    # GET /alerts/new
    def new
      @alert = Alert.new
    end

    # GET /alerts/1/edit
    def edit
    end

    # POST /alerts
    def create
      @alert = Alert.new(alert_params)
      if @alert.save
        redirect_to @alert, notice: 'Alert was successfully created.'
      else
        render action: 'new'
      end
    end

    # PATCH/PUT /alerts/1
    def update
      if @alert.update_attributes(alert_params)
        redirect_to @alert, notice: 'Alert was successfully updated.'
      else
        render action: 'edit'
      end
    end

    # DELETE /alerts/1
    def destroy
      @alert.destroy
      redirect_to alerts_url, notice: 'Alert was successfully destroyed.'
    end

  private

    def set_alert
      @alert = Alert.find(params[:id])
    end

    def alert_params
      params.require(:alert).permit(
        :id,  :_destroy,
        :page_id,
        :greater_than,
        :average,
        :sigma,
        :text_message
      ).tap do |whitelisted| 
        text_numbers = params[:alert][:text_numbers]
        whitelisted[:text_numbers] = to_array(params[:alert][:text_numbers])
        whitelisted[:emails] =  to_array(params[:alert][:emails])
      end
    end

    def to_array array_string
      array_string.gsub(" ","")[1..(array_string.length-2)].split(",")
    end
  end
end
