require_dependency "twilio_surveyor_stats/application_controller"

module TwilioSurveyorStats
  class SurveyController < TwilioSurveyor::SurveyController

    def call_terminated
      call = TwilioSurveyor::CallManager.end_call(params[:CallSid], params[:CallDuration])
      TwilioSurveyor::CallStats.update_all_stats(call) unless call.nil?
      clear_session
      render(xml: {status: "call terminated" }.to_xml, layout: false)
    end

  private
  
    def save_response
      unless params[:Digits].nil? && params[:recording_url].nil?
        save_organization if current_page.response_type == "organization"
        
        answer = TwilioSurveyor::Answer.create({
            digits: params[:Digits],
            recording_url: params[:RecordingUrl],
            recording_duration: params[:RecordingDuration],
            recording_sid: params[:RecordingSid],
            response_duration: get_response_duration,
            doc: current_doc,
            page: current_page,
            call: current_call
          })
        TwilioSurveyor::Alert.check_and_send_alert(answer.page)
        answer
      end
    end

  end
end
