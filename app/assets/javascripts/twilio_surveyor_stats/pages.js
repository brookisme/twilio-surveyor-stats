
jQuery(document).ready(function($) {

  page_graph = $('#page_graph')

  if ( (typeof(data) !== "undefined") && page_graph.length){
    if ( (page_graph.data("type")=="frequency") || (page_graph.data("type")=="menu") ) frequencyChart()
    else if (page_graph.data("type")=="average") frequencyChart()
    else if (page_graph.data("type")=="record") areaGraph()
    else if (page_graph.data("type")=="area") areaGraph()
    else if (page_graph.data("type")=="scatter") scatterPlot()
  }

})

var scatterPlot = function() {


  var parseDate = d3.time.format("%Y%m%d").parse;

  data.forEach(function(d) {
    d.date = parseDate(d.xval.toString())
    // d.yval = +d.yval;

    d.xval = d.date
  });


  var chart_height = 200
  var chart_width = 425
  var margin = {top: 20, right: 20, bottom: 30, left: 40},
      width = chart_width - margin.left - margin.right,
      height = chart_height - margin.top - margin.bottom;


var x = d3.time.scale()
    .range([0, width]);

var y = d3.scale.linear()
    .range([height, 0]);

var color = d3.scale.category10();


 // var xAxis = d3.svg.axis().scale(x).ticks(d3.time.months, 1).tickSize(-5).tickSubdivide(true);

var xAxis = d3.svg.axis()
    .scale(x)
    .orient('bottom')
    .ticks(d3.time.months, 1)
    .tickFormat(d3.time.format('%b %y'))
    .tickSize(0)

var yAxis = d3.svg.axis()
    .scale(y)
    .orient("left");

var svg = d3.select("#page_graph").append("svg")
    .attr("width", width + margin.left + margin.right)
    .attr("height", height + margin.top + margin.bottom)
  .append("g")
    .attr("transform", "translate(" + margin.left + "," + margin.top + ")");


  x.domain(d3.extent(data, function(d) { return d.xval; })).nice();
  y.domain(d3.extent(data, function(d) { return d.yval; })).nice();


 // Add the x-axis.
 svg.append("g")
 .attr("class", "x axis")
 .attr("x", width)
 .attr("transform", "translate(0," + height + ")")
 .call(xAxis);

  // svg.append("g")
  //     .attr("class", "x axis")
  //     .attr("transform", "translate(0," + height + ")")
  //     .call(xAxis)

  svg.append("g")
      .attr("class", "y axis")
      .call(yAxis)
    // .append("text")
    //   .attr("class", "label")
    //   .attr("transform", "rotate(-90)")
    //   .attr("y", 6)
    //   .attr("dy", ".71em")
    //   .style("text-anchor", "end")
    //   .text("Sepal Length (cm)")

  svg.selectAll(".dot")
      .data(data)
    .enter().append("circle")
      .attr("class", "dot")
      .attr("r", 5)
      .attr("cx", function(d) { return x(d.xval); })
      .attr("cy", function(d) { return y(d.yval); })
      .style("fill", function(d) { return color(d.yval)});

  // var legend = svg.selectAll(".legend")
  //     .data(color.domain())
  //   .enter().append("g")
  //     .attr("class", "legend")
  //     .attr("transform", function(d, i) { return "translate(0," + i * 20 + ")"; });

  // legend.append("rect")
  //     .attr("x", width - 18)
  //     .attr("width", 18)
  //     .attr("height", 18)
  //     .style("fill", color);

  // legend.append("text")
  //     .attr("x", width - 24)
  //     .attr("y", 9)
  //     .attr("dy", ".35em")
  //     .style("text-anchor", "end")
  //     .text(function(d) { return d; });


}



var areaGraph = function() {
  var chart_height = 200
  var chart_width = 425
  var margin = {top: 20, right: 20, bottom: 30, left: 40},
      width = chart_width - margin.left - margin.right,
      height = chart_height - margin.top - margin.bottom;

// var parseDate = d3.time.format("%d-%b-%y").parse;

var x = d3.scale.linear()
    .range([0, width]);

var y = d3.scale.linear()
    .range([height, 0]);

var xAxis = d3.svg.axis()
    .scale(x)
    .orient("bottom");

var yAxis = d3.svg.axis()
    .scale(y)
    .orient("left");

var area = d3.svg.area()
    .x(function(d) { return x(d.xval); })
    .y0(height)
    .y1(function(d) { return y(d.yval); });

var svg = d3.select("#page_graph").append("svg")
    .attr("width", width + margin.left + margin.right)
    .attr("height", height + margin.top + margin.bottom)
  .append("g")
    .attr("transform", "translate(" + margin.left + "," + margin.top + ")");


  x.domain(d3.extent(data, function(d) { return d.xval; }));
  y.domain([0, d3.max(data, function(d) { return d.yval; })]);

  svg.append("path")
      .datum(data)
      .attr("class", "area")
      .attr("d", area);

  svg.append("g")
      .attr("class", "x axis")
      .attr("transform", "translate(0," + height + ")")
      .call(xAxis);

  svg.append("g")
      .attr("class", "y axis")
      .call(yAxis)
    .append("text")
      .attr("transform", "rotate(-90)")
      .attr("y", 6)
      .attr("dy", ".71em")
      .style("text-anchor", "end")
      .text("");


}












var frequencyChart = function() {
  var chart_height = 200
  var chart_width = 425
  var margin = {top: 20, right: 20, bottom: 30, left: 40},
      width = chart_width - margin.left - margin.right,
      height = chart_height - margin.top - margin.bottom;

  var formatPercent = d3.format(".0%");

  var x = d3.scale.ordinal()
      .rangeRoundBands([0, width], .1);

  var y = d3.scale.linear()
      .range([height, 0]);

  var xAxis = d3.svg.axis()
      .scale(x)
      .orient("bottom");

  var yAxis = d3.svg.axis()
      .scale(y)
      .orient("left")
      .tickFormat(formatPercent);

  var svg = d3.select("#page_graph").append("svg")
      .attr("width", width + margin.left + margin.right)
      .attr("height", height + margin.top + margin.bottom)
      .append("g")
      .attr("transform", "translate(" + margin.left + "," + margin.top + ")");


  x.domain(data.map(function(d) { return d.xval; }));
  y.domain([0, d3.max(data, function(d) { return d.yval; })]);

  svg.append("g")
      .attr("class", "x axis")
      .attr("transform", "translate(0," + height + ")")
      .call(xAxis);

  svg.append("g")
      .attr("class", "y axis")
      .call(yAxis)
    .append("text")
      .attr("transform", "rotate(-90)")
      .attr("y", 6)
      .attr("dy", ".51em")
      .style("text-anchor", "end")
      .text("Frequency");

  svg.selectAll(".bar")
      .data(data)
    .enter().append("rect")
      .attr("class", "bar")
      .attr("x", function(d) { return x(d.xval); })
      .attr("width",  x.rangeBand())
      .attr("y", function(d) { return y(d.yval); })
      .attr("height", function(d) { return height - y(d.yval); });

  function type(d) {
    d.yval = +d.yval;
    return d;
  }

}






var durationGraph = function() {
  lineGraph()
}
var lineGraph = function() {

  var chart_height = 200
  var chart_width = 425
  var margin = {top: 20, right: 20, bottom: 30, left: 40},
      width = chart_width - margin.left - margin.right,
      height = chart_height - margin.top - margin.bottom;


  var x = d3.scale.linear()
      .range([0, width]);

  var y = d3.scale.linear()
      .range([height, 0]);

  var xAxis = d3.svg.axis()
      .scale(x)
      .orient("bottom");

  var yAxis = d3.svg.axis()
      .scale(y)
      .orient("left");

  var line = d3.svg.line()
      .x(function(d) { return x(d.xval); })
      .y(function(d) { return y(d.yval); });

  var svg = d3.select("#page_graph").append("svg")
      .attr("width", width + margin.left + margin.right)
      .attr("height", height + margin.top + margin.bottom)
    .append("g")
      .attr("transform", "translate(" + margin.left + "," + margin.top + ")");

    data.forEach(function(d) {
      d.yval = +d.yval;
    });

    x.domain(d3.extent(data, function(d) { return d.xval; }));
    y.domain(d3.extent(data, function(d) { return d.yval; }));

    svg.append("g")
        .attr("class", "x axis")
        .attr("transform", "translate(0," + height + ")")
        .call(xAxis);

    svg.append("g")
        .attr("class", "y axis")
        .call(yAxis)

    svg.append("path")
        .datum(data)
        .attr("class", "line")
        .attr("d", line);

}