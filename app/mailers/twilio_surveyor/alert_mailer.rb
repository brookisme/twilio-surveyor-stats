module TwilioSurveyor
  class AlertMailer < ActionMailer::Base
    def alert_notification(email,alert)
      @message = alert.email_message || alert.text_message 
      @page_reference_name = alert.page.reference_name
      mail(
        from: 'alerts@laborvoices.com',
        to: email,
        subject: 'LaborVoices:  Account Alert'
      )
    end
  end
end
