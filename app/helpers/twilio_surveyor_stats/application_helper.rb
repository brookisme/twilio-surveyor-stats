module TwilioSurveyorStats
  module ApplicationHelper

    # presenters

    def present(object, klass = nil)
      klass ||= "#{object.class}Presenter".constantize
      presenter = klass.new(object, self)
      yield presenter if block_given?
      presenter
    end

  end
end
