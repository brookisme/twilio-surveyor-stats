module TwilioSurveyor
  class OrganizationStatsPresenter < OrganizationPresenter
    include BaseStatsPresenter

    def rating
      number_with_precision(stats.rating, precision: 2) unless stats.rating.nil?
    end

    def sigma_score
      number_with_precision(stats.sigma_score, precision: 2) unless stats.sigma_score.nil?
    end

    def use_score
      number_with_precision(stats.use_score, precision: 2) unless stats.use_score.nil?
    end

    def reliability_score
      number_with_precision(stats.reliability_score, precision: 2) unless stats.reliability_score.nil?
    end
  end
end