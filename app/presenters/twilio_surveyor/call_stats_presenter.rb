module TwilioSurveyor
  class CallStatsPresenter < CallPresenter
    include BaseStatsPresenter


    def self.total_duration(template,group_id=nil)
      total = CallStats.total_duration(group_id)
      template.distance_of_time_in_words(total) unless total.nil? 
    end

    def self.average_duration(template,group_id=nil)
      average = CallStats.average_duration(group_id)
      template.distance_of_time_in_words(average) unless average.nil? 
    end

    def self.total_recording_duration(template,group_id=nil)
      total = Call.total_recording_duration(group_id)
      template.distance_of_time_in_words(total) unless total.nil?
    end

    def self.average_recording_duration(template,group_id=nil)
      average = Call.average_recording_duration(group_id)
      template.distance_of_time_in_words(average) unless average.nil?
    end

    def sigma_difference
      unless stats.nil?
        number_with_precision(stats.average_sigma_difference.to_s, precision: 2)
      end
    end

    def score
      unless stats.nil?
        if stats.has_score?
          number_with_precision(stats.average_score.to_s, precision: 2)
        else
          "&empty;".html_safe
        end
      end
    end

  end
end