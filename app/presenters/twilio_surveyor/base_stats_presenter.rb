module TwilioSurveyor
  module BaseStatsPresenter

    def stats stats
      @stats = stats
    end

    def stats
      @stats ||= fetch_stats
    end

    def object_class
      @object_class ||= "#{@object.class}Stats".constantize
    end

  private

    def fetch_stats
      object_stats = (object_class.send(fetch_method_name,@object.id) if object_class.respond_to?(fetch_method_name))
      if object_stats.nil?
        object_stats = object_class.new
        object_stats.update_attribute(class_type,@object)
      end
      object_stats
    end

    def class_type
      @object.class.name.gsub("TwilioSurveyor::","").downcase
    end

    def fetch_method_name
      "by_"+class_type+"_id"
    end

  end
end