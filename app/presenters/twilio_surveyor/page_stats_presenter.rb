module TwilioSurveyor
  class PageStatsPresenter < PagePresenter
    include BaseStatsPresenter

    def average
      number_with_precision(stats.average.to_s, precision: 2) if page.is_numeric?
    end
    def sigma
      number_with_precision(stats.sigma.to_s, precision: 2) if page.is_numeric?
    end
  end
end