module TwilioSurveyorStats
  class AlertPresenter < TwilioSurveyor::BasePresenter 
    presents :alert

    def page_name
      alert.page.reference_name unless alert.page.nil?
    end
    def doc_name
      alert.doc.reference_name unless alert.doc.nil?
    end
    def organization_name
      alert.orgainzation.reference_name unless alert.organization.nil?
    end
    def group_name
      alert.group.reference_name unless alert.group.nil?
    end 
  end  
end