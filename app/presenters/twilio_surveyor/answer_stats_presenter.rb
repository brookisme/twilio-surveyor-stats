module TwilioSurveyor
  class AnswerStatsPresenter < AnswerPresenter
    include BaseStatsPresenter


    def sigma_difference
      number_with_precision(stats.sigma_difference.to_s, precision: 2)
    end

    def score
      if stats.has_score?
        number_with_precision(stats.score.to_s, precision: 2)
      else
        "&empty;".html_safe
      end
    end

  private

    def fetch_stats
      AnswerStats.new(answer)
    end

  end
end