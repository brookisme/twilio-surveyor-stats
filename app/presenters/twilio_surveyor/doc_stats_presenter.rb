module TwilioSurveyor
  class DocStatsPresenter < DocPresenter
    include BaseStatsPresenter

    attr_reader :for_organization_id

    def sigma_data
      DocStats.all_by_doc_id(doc.id).collect {
        |stats| 
        unless stats.average_sigma.nil?
          { xval: stats.created_at.to_date.to_formatted_s(:number).to_i, yval: stats.average_sigma } 
        end
      }.compact.to_json
    end

    def number_of_organizations
      doc.organizations.count.to_s unless doc.organizations.nil?
    end

    def number_of_calls
      doc.calls.count.to_s unless doc.calls.nil?
    end

    def average_sigma
      number_with_precision(stats.average_sigma.to_s, precision: 2)
    end

    def average_reliability
      number_with_precision(stats.average_reliability.to_s, precision: 2)
    end

    #  by organization

    def for_organization_id=(organization_id)
      @for_organization_id = organization_id
      @doc_stats_by_organization =  nil
    end

    def number_of_calls_for_organization
      Call.by_doc_id(doc.id).by_organization_id(for_organization_id).count
    end

    def doc_stats_by_organization
      @doc_stats_by_organization ||= DocStats.by_doc_id_and_organization_id(doc.id,for_organization_id)
    end

    def average_sigma_for_organization
      number_with_precision(doc_stats_by_organization.average_sigma.to_s, precision: 2) unless doc_stats_by_organization.nil?
    end

    def average_reliability_for_organization
      number_with_precision(doc_stats_by_organization.average_reliability.to_s, precision: 2) unless doc_stats_by_organization.nil?
    end

  end
end