module TwilioSurveyor
  class UserStatsPresenter < UserPresenter
    include BaseStatsPresenter


    def average_sigma_difference
      number_with_precision(stats.average_sigma_difference.to_s, precision: 2)
    end

    def average_score
      if stats.has_score?
        number_with_precision(stats.average_score.to_s, precision: 2)
      else
        "&empty;".html_safe
      end
    end
  end
end