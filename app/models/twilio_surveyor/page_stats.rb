module TwilioSurveyor
  class PageStats < ActiveRecord::Base
    include StatsCalculator

    belongs_to :page
    belongs_to :organization

    def self.by_page_id page_id
      self.by_page_id_and_organization_id(page_id,nil)
    end

    def self.by_page_id_and_organization_id page_id, organization_id
      where(page_id: page_id).where(organization_id: organization_id).last
    end

    def self.update_everything!(organization=nil)
      Page.all.each do |page|
        PageStats.update_stats page, organization
      end
    end

    def self.update_stats page, organization = nil
      (page = Page.find(page)) if page.class.name == "Fixnum"
      stats = PageStats.new
      stats.organization = organization
      stats.page = page
      stats.average = stats.calculate_average
      stats.save
      stats.sigma = stats.calculate_sigma
      stats.reliability = stats.calculate_reliability
      stats if stats.save
    end

    def calculate_average
      if page.is_numeric?
        digits = page.answers_by_organization_id(organization_id).collect{ |answers| answers.digits.to_f }
        digits.sum.to_f/digits.length
      end
    end

    def calculate_sigma
      if page.is_numeric?
        diffs_to_sigma answer_stats_array.collect{ |answer_stats| answer_stats.difference.to_f }
      end
    end

    def calculate_reliability
      if page.is_numeric? && page.has_tag?("control")
        unless page.correct_answer.nil? || page.correct_answer < 0
          attribute_average answer_stats_array, "score"
        end
      end
    end

    def answer_stats_array
      @answer_stats_array ||= get_answer_stats_array
    end

    def answer_occurrences answer_digits
      if page.is_numeric?
        page.answers.pluck(:digits).count(answer_digits.to_s)
      end
    end

    def occurrence_array options=nil
      if page.is_numeric?
        options = (0..9) if options.nil?
        options.collect{ |option| answer_occurrences(option) }
      end
    end

    def average_array options=nil
      if page.is_numeric?
        options = (0..9) if options.nil?
        options.collect{ |option| { xval: option, yval: (answer_occurrences(option).to_f/page.answers.count) } }
      end
    end

    def lg_array options=nil
      if page.is_numeric?
        options = (0..9) if options.nil?
        options.collect{ |option| { xval: option, yval: (answer_occurrences(option)) } }
      end
    end

    def duration_array
      page.answers.collect.with_index do |answer,index|
        { xval: index, yval: answer.recording_duration }
      end
    end

  private

    def get_answer_stats_array
      if page.is_numeric? 
        page.answers_by_organization_id(organization_id).collect { |answer| AnswerStats.new(answer,organization) }
      end
    end

    def organization_id
      organization.id unless organization.nil?
    end

  end
end 
