module TwilioSurveyor
  module StatsCalculator

    #
    # scores
    #

    def calculate_sigma_score
      unless num_users == 0 || doc_stats_array.blank?
        sigma_avg = attribute_average doc_stats_array, "average_sigma"
        (1 - (sigma_best - sigma_avg).abs/sigma_avg) unless sigma_avg.nil?
      end
    end

    def calculate_reliability_score
      unless num_users == 0 || doc_stats_array.blank?
        attribute_average(doc_stats_array, "average_reliability")
      end
    end

    def calculate_use_score
      unless num_users == 0
        (num_calls - num_users).to_f/(num_calls+num_users) unless num_users == 0
      end
    end

    def calculate_rating
      sum = 0
      sum += reliability_coefficient * reliability_score unless reliability_score.nil?
      sum += use_coefficient * use_score unless use_score.nil?
      sum += sigma_coefficient * sigma_score unless sigma_score.nil?
      sum / rating_normalization
    end

    #
    #  Constants and Coefficients
    #

    def reliability_coefficient
      15
    end

    def use_coefficient
      5
    end

    def sigma_coefficient
      10
    end

    def sigma_best
      1
    end

    def rating_normalization 
      (reliability_coefficient + use_coefficient + sigma_coefficient)
    end

    #
    # helpers
    #

    def attribute_average array, attribute
      unless array.nil?
        array = array.compact.collect{ |object|  object.send(attribute).to_f unless object.send(attribute).nil? }.compact
        array.sum.to_f/array.length if (!array.nil? && array.length > 0)
      end
    end

    def diffs_to_sigma diffs_array
      unless diffs_array.nil?
        diffs_squared = diffs_array.compact.collect{ |diff| diff.to_f * diff.to_f }
        Math.sqrt( diffs_squared.sum/(diffs_squared.length-1) ) if diffs_squared.length > 1
      end
    end

  end
end