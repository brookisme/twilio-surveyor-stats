module TwilioSurveyor
  class DocStats < ActiveRecord::Base
    include StatsCalculator

    belongs_to :doc
    belongs_to :organization

    def self.all_by_doc_id doc_id
      self.all_by_doc_id_and_organization_id(doc_id,nil)
    end

    def self.all_by_doc_id_and_organization_id doc_id, organization_id
      where(doc_id: doc_id).where(organization_id: organization_id)
    end

    def self.by_doc_id doc_id
      self.by_doc_id_and_organization_id(doc_id,nil)
    end

    def self.by_doc_id_and_organization_id doc_id, organization_id
      self.all_by_doc_id_and_organization_id(doc_id,organization_id).last
    end

    def self.update_everything!(organization=nil)
      Doc.all.each do |doc|
        DocStats.update_stats doc, organization
      end
    end

    def self.update_stats doc, organization = nil
      (doc = Doc.find(doc)) if doc.class.name == "Fixnum"
      stats = DocStats.new
      stats.organization = organization
      stats.doc = doc
      stats.average_sigma = stats.calculate_average_sigma
      stats.average_reliability = stats.calculate_average_reliability
      stats if stats.save
    end

    def self.average_sigma
      array = Doc.all.collect{ |doc| ds = DocStats.by_doc_id(doc.id); ds.average_sigma unless ds.nil? }.compact
      array.sum.to_f/array.length if (!array.nil? && array.length > 0)
    end

    def self.average_reliability
      array = Doc.all.collect{ |doc| ds = DocStats.by_doc_id(doc.id); ds.average_reliability unless ds.nil?  }.compact
      array.sum.to_f/array.length if (!array.nil? && array.length > 0)    
    end

    def page_stats_array
      @page_stats_array ||= get_page_stats_array
    end

    def calculate_average_sigma
      attribute_average page_stats_array, "sigma"
    end

    def calculate_average_reliability
      attribute_average page_stats_array, "reliability"
    end

    def has_reliability?
      !average_reliability.nil?
    end

  private

    def get_page_stats_array
      doc.gathers.collect{ |page| PageStats.by_page_id_and_organization_id(page.id,organization_id) }
    end

    def organization_id
      organization.id unless organization.nil?
    end


  end
end 
