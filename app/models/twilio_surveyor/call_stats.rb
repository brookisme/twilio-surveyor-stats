module TwilioSurveyor
  class CallStats < ActiveRecord::Base
    include StatsCalculator

    belongs_to :call
    attr_accessor :organization_only

    def self.by_call_id call_id
      self.by_call_id_and_organization_id(call_id,nil)
    end

    def self.by_call_id_and_organization_id call_id, organization_id
      where(call_id: call_id).where(organization_id: organization_id).last
    end

    def self.update_everything!(organization=nil)
      Call.all.each do |call|
        CallStats.update_stats call, organization
      end
    end

    def self.update_all_stats call
      (call = Call.find(call)) if call.class.name == "Fixnum"
 
      CallStats.update_stats call
      unless call.docs.blank?
        call.docs.each do |doc|
          DocStats.update_stats doc
          doc.pages.each{ |p| PageStats.update_stats(p) }
        end
      end

      UserStats.update_stats call.user unless call.user.nil?
      OrganizationStats.update_stats call.organization unless call.organization.nil?
      GroupStats.update_stats call.group unless call.group.nil?

      unless call.organization.nil?
        CallStats.update_stats call, call.organization
        unless call.docs.blank?
          call.docs.each do |doc|
            DocStats.update_stats doc,call.organization
            doc.pages.each{ |p| PageStats.update_stats(p,call.organization) }
          end
        end
      end
    end

    def self.update_stats call, organization = nil
      if organization.nil? || organization == call.organization
        (call = Call.find(call)) if call.class.name == "Fixnum"
        stats = CallStats.new
        stats.organization_only = !organization.nil?
        stats.call = call
        stats.average_difference = stats.calculate_average_difference
        stats.average_sigma_difference = stats.calculate_average_sigma_difference
        stats.average_score = stats.calculate_average_score
        stats if stats.save
      end
    end

    def self.total_duration(group_id=nil)
      self.duration_array(group_id).sum.to_f
    end

    def self.average_duration(group_id=nil)
      self.total_duration(group_id)/self.duration_array.length unless self.duration_array.blank?
    end

    def self.average_score
      array = Call.all.collect{ |call| CallStats.by_call_id(call.id).average_score }.compact
      array.sum.to_f/array.length if (!array.nil? && array.length > 0)    
    end

    def answer_stats_array
      @answer_stats_array || get_answer_stats_array
    end

    def calculate_average_difference
      attribute_average answer_stats_array, "difference"
    end

    def calculate_average_sigma_difference
      attribute_average answer_stats_array, "sigma_difference"
    end

    def calculate_average_score
      attribute_average answer_stats_array, "score"
    end

    def has_score?
      !average_score.nil?
    end

  private
    
    def self.duration_array(group_id=nil)
      Call.select(:duration).by_group_id(group_id).where.not(duration: nil).collect{ |c| c.duration.to_i }
    end
    
    def get_answer_stats_array
      org = call.organization if @organization_only
      call.gathers.collect { |answer| AnswerStats.new(answer,org) }
    end
  end
end
