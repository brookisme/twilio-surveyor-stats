module TwilioSurveyor
  class UserStats < ActiveRecord::Base
    include StatsCalculator

    belongs_to :user

    def self.by_user_id user_id
      where(user_id: user_id).last
    end

    def self.update_everything!
      User.all.each do |user|
        UserStats.update_stats user
      end
    end

    def self.update_stats user
      (user = User.find(user)) if user.class.name == "Fixnum"
      stats = UserStats.new
      stats.user = user
      stats.average_difference = stats.calculate_average_difference
      stats.average_sigma_difference = stats.calculate_average_sigma_difference
      stats.average_score = stats.calculate_average_score
      stats.save
    end

    def answer_stats_array
      @answer_stats_array || get_answer_stats_array
    end

    def calculate_average_difference
      attribute_average answer_stats_array, "difference"
    end

    def calculate_average_sigma_difference
      attribute_average answer_stats_array, "sigma_difference"
    end

    def calculate_average_score
      attribute_average answer_stats_array, "score"
    end

    def has_score?
      !average_score.nil?
    end

  private

    def get_answer_stats_array
      user.gathers.collect{ |answer| AnswerStats.new(answer) }
    end

  end
end
