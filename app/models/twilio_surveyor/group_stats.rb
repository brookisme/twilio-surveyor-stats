module TwilioSurveyor
  class GroupStats < ActiveRecord::Base
    include StatsCalculator

    belongs_to :group

    def self.by_group_id group_id
      where(group_id: group_id).last
    end

    def self.update_everything!
      Group.all.each do |group|
        GroupStats.update_stats group
      end
    end

    def self.update_stats group
      (group = Group.find(group)) if group.class.name == "Fixnum"
      stats = GroupStats.new
      stats.group = group
      stats.sigma_score = stats.calculate_sigma_score
      stats.reliability_score = stats.calculate_reliability_score
      stats.use_score = stats.calculate_use_score
      stats.rating = stats.calculate_rating
      stats.save
    end

    def doc_stats_array
      @doc_stats_array ||= get_doc_stats_array
    end

    def num_calls
      @num_calls ||= group.calls.count
    end

    def num_users
      @num_users ||= group.users.count
    end

  private

    def get_doc_stats_array
      group.docs.collect{ |doc| DocStats.by_doc_id(doc.id) } unless group.docs.nil?
    end

  end
end
