module TwilioSurveyor
  class Alert < ActiveRecord::Base
    belongs_to :page
    belongs_to :group

    def self.by_page_id page_id
       relation = where(page_id: page_id).order(id: :desc).limit(1)
       relation.first unless relation.nil?
    end

    def self.check_and_send_alert page
      alert = by_page_id(page.id)
      unless alert.nil?
        alert.send_alert if alert.need_to_send_alert?
      end
    end

    def doc
      page.doc unless page.nil?
    end

    def need_to_send_alert?
      page_stats = PageStats.new(page)
      if greater_than
        check_average = page_stats.average >= average
      else
        check_average = page_stats.average <= average
      end
      check_average && (page_stats.sigma <= sigma)
    end

    def send_alert
      if last_sent.nil? || ((Time.now - last_sent) > 1.day)
        send_texts
        send_emails
        self.last_sent = Time.now
        self.save
      end
    end

    def send_texts
      text_numbers.each do |number|
        CallManager.text(text_message, number, outgoing_number)
      end
    end
    
    def send_emails
      Rails.logger.debug "DEBUG SENDING EMAILS " + emails.to_s
      emails.each do |email|
        AlertMailer.alert_notification(email,self)
      end
    end

    def outgoing_number
      if group.nil? || group.outgoing_number.nil?
        TwilioSurveyor.config["caller_id"]
      else
        group.outgoing_number
      end
    end

  end
end
