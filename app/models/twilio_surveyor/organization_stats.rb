module TwilioSurveyor
  class OrganizationStats < ActiveRecord::Base
    include StatsCalculator

    belongs_to :organization

    def self.by_organization_id organization_id
      where(organization_id: organization_id).last
    end

    def self.update_everything!
      Organization.all.each do |organization|
        OrganizationStats.update_stats organization
      end
    end

    def self.update_stats organization
      (organization = Organization.find(organization)) if organization.class.name == "Fixnum"
      stats = OrganizationStats.new
      stats.organization = organization
      stats.sigma_score = stats.calculate_sigma_score
      stats.reliability_score = stats.calculate_reliability_score
      stats.use_score = stats.calculate_use_score
      stats.rating = stats.calculate_rating
      stats.save
    end

    def doc_stats_array
      @doc_stats_array ||= get_doc_stats_array
    end

    def num_calls
      @num_calls ||= organization.calls.count
    end

    def num_users
      @num_users ||= organization.users.count
    end

  private

    def get_doc_stats_array
      organization.group_docs.collect{ |doc| DocStats.by_doc_id(doc.id) } unless organization.group_docs.nil?
    end

  end
end
