module TwilioSurveyor
  class AnswerStats
    include StatsCalculator
    
    attr_reader :answer, :organization

    def initialize(answer,organization = nil)
      @answer = answer
      @organization = organization
    end

    def page_stats
      @page_stats ||= get_page_stats
    end

    def difference
      @difference ||= calculate_difference
    end

    def sigma_difference
      @sigma_difference ||= calculate_sigma_difference
    end

    def score
      @score ||= calculate_score
    end

    def has_score?
      @answer.page_has_tag?("control")
    end

  private

    def get_page_stats
      if @answer.page_is_numeric?
        page_stats = PageStats.by_page_id_and_organization_id(@answer.page.id,organization_id)
      end    
    end

    def calculate_difference
      if @answer.page_is_numeric? && 
        !page_stats.nil? && 
        !page_stats.average.nil?
        (@answer.digits.to_f - page_stats.average).abs
      end
    end

    def calculate_sigma_difference
      if @answer.page_is_numeric? && 
        !page_stats.nil? && 
        !page_stats.sigma.nil? && 
        page_stats.sigma != 0

        difference.to_f/page_stats.sigma
      end
    end

    def calculate_score
      if @answer.page_is_numeric? && has_score?
        if @answer.page.correct_answer === @answer.digits
          1
        else
          0
        end
      end
    end

    def organization_id
      organization.id unless organization.nil?
    end
  end
end
