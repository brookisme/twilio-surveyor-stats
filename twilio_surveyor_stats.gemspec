$:.push File.expand_path("../lib", __FILE__)

# Maintain your gem's version:
require "twilio_surveyor_stats/version"

# Describe your gem and declare its dependencies:
Gem::Specification.new do |s|
  s.name        = "twilio_surveyor_stats"
  s.version     = TwilioSurveyorStats::VERSION
  s.authors     = ["Brook Williams"]
  s.email       = ["brook@stickandlogdesigns.com"]
  s.homepage    = "http://stickandlogdesigns.com"
  s.summary     = "Stats: An engine for creating user accounts and surveys with Twilio"
  s.description = "Stats: A MVC structure for creating Twilml and interfacing with Twilio's rest API.  An admin system is generated for easy management"

  s.files = Dir["{app,config,db,lib}/**/*", "MIT-LICENSE", "Rakefile", "README.rdoc"]

  s.add_dependency "rails", "~> 4.0.0"

  s.add_development_dependency "pg"
end
