class CreateTwilioSurveyorCallStats < ActiveRecord::Migration
  def change
    create_table :twilio_surveyor_call_stats do |t|
      t.references :call, index: true
      t.float :average_difference
      t.float :average_sigma_difference
      t.float :average_score

      t.timestamps
    end
  end
end
