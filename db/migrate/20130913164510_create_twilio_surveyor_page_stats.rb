class CreateTwilioSurveyorPageStats < ActiveRecord::Migration
  def change
    create_table :twilio_surveyor_page_stats do |t|
      t.references :page, index: true
      t.float :average
      t.float :sigma
      t.float :reliability

      t.timestamps
    end
  end
end
