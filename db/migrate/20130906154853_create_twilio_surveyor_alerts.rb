class CreateTwilioSurveyorAlerts < ActiveRecord::Migration
  def change
    create_table :twilio_surveyor_alerts do |t|
      t.references :page, index: true
      t.boolean :greater_than
      t.float :average
      t.float :sigma
      t.timestamp :last_sent
      t.string :text_numbers, array: true
      t.string :emails, array: true
      t.string :text_message

      t.timestamps
    end
  end
end
