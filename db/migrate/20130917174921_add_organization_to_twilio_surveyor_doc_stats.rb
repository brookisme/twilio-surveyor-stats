class AddOrganizationToTwilioSurveyorDocStats < ActiveRecord::Migration
  def change
    add_reference :twilio_surveyor_doc_stats, :organization, index: true
  end
end
