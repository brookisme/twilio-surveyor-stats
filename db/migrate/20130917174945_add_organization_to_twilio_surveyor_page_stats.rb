class AddOrganizationToTwilioSurveyorPageStats < ActiveRecord::Migration
  def change
    add_reference :twilio_surveyor_page_stats, :organization, index: true
  end
end
