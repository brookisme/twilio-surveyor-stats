class AddGroupToTwilioSurveyorAlerts < ActiveRecord::Migration
  def change
    add_reference :twilio_surveyor_alerts, :group, index: true
  end
end
