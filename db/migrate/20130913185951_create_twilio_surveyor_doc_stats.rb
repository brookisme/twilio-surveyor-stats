class CreateTwilioSurveyorDocStats < ActiveRecord::Migration
  def change
    create_table :twilio_surveyor_doc_stats do |t|
      t.references :doc, index: true
      t.float :average_sigma
      t.float :average_reliability

      t.timestamps
    end
  end
end
