class AddEmailMessageToTwilioSurveyorAlerts < ActiveRecord::Migration
  def change
    add_column :twilio_surveyor_alerts, :email_message, :text
  end
end
