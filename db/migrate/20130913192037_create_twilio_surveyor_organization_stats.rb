class CreateTwilioSurveyorOrganizationStats < ActiveRecord::Migration
  def change
    create_table :twilio_surveyor_organization_stats do |t|
      t.references :organization, index: true
      t.float :sigma_score
      t.float :reliability_score
      t.float :use_score
      t.float :rating

      t.timestamps
    end
  end
end
