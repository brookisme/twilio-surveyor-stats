class CreateTwilioSurveyorGroupStats < ActiveRecord::Migration
  def change
    create_table :twilio_surveyor_group_stats do |t|
      t.references :group, index: true
      t.float :sigma_score
      t.float :reliability_score
      t.float :use_score
      t.float :rating

      t.timestamps
    end
  end
end
