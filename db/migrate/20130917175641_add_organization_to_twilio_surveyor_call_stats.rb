class AddOrganizationToTwilioSurveyorCallStats < ActiveRecord::Migration
  def change
    add_reference :twilio_surveyor_call_stats, :organization, index: true
  end
end
