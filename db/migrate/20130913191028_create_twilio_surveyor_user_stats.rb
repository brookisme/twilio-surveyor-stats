class CreateTwilioSurveyorUserStats < ActiveRecord::Migration
  def change
    create_table :twilio_surveyor_user_stats do |t|
      t.references :user, index: true
      t.float :average_difference
      t.float :average_sigma_difference
      t.float :average_score

      t.timestamps
    end
  end
end
