# Dummy Data
@lorem_ipsum = [
  "Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat.",
  "Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat.",
  "Duis autem vel eum iriure dolor in hendrerit in vulputate velit esse molestie consequat, vel illum dolore eu feugiat nulla facilisis at vero eros et accumsan et iusto odio dignissim",
  "Qui blandit praesent luptatum zzril delenit augue duis dolore te feugait nulla facilisi. Nam liber tempor cum soluta nobis eleifend option",
  "congue nihil imperdiet doming id quod mazim placerat facer possim assum. Typi non habent claritatem insitam; est usus legentis in iis qui facit eorum claritatem.",
  "Investigationes demonstraverunt lectores legere me lius quod ii legunt saepius. Claritas est etiam processus dynamicus, qui sequitur mutationem consuetudium lectorum.",
  "Mirum est notare quam littera gothica, quam nunc putamus parum claram, anteposuerit litterarum formas humanitatis per seacula quarta decima et quinta decima.",
  "Eodem modo typi, qui nunc nobis videntur parum clari, fiant sollemnes in futurum."
]
@registration_survey = [
  {
    response_type: 'message',
    question: 'Thank you for calling Labor Voices.  During this call we will ask you a few registration questions.   After you are registered and in our system you may call us anytime to take a survey or report a greivence.'
  },
  {
    response_type: 'info',
    question: 'What is your phone number',
  },
  {
    response_type: 'record',
    question: 'Tell us the name of the factory you work in',
  },
  {
    response_type: 'average',
    length: 2,
    question: 'How many years have you worked in this factory?  press the pound sign when finished.'
  },
  {
    response_type: 'record',
    question: 'Tell us the name of your supervisor'
  },
  {
    response_type: 'average',
    length: 2,
    question: 'How old are you?  press the pound sign when finished'
  },
  {
    response_type: 'average',
    length: 5,
    question: 'What is your monthly wage in taka? press the pound sign when finished'
  },
  {
    response_type: 'record',
    question: 'Is there anything else you would like to report to us at this time',
  },
  {
    response_type: 'message',
    question: 'Thank you for your help.  You are now registered with Labor Voices'
  }
]

@greievence_survey = [
  {
    response_type: 'message',
    question: 'You have reached LaborVoices grievence handling system.'
  },
  {
    response_type: 'frequency',
    question: 'Please enter 0 thourgh 9 to rate the severity of the problem you are reporting. 0 not very important.  9 extremely important',
  },
  {
    response_type: 'average',
    question: 'Are your or other workers in immediate danger?  Please 1 for yes or 2 for no'
  },
  {
    response_type: 'record',
    question: 'Describe the problem you are reporting',
  },
  {
    response_type: 'message',
    question: 'Thank you for your help.  Our team will look into your issue right away.'
  }
]
@pages = [
  {
    response_type: 'frequency',
    question: 'how many emergency exits are there',
  },
  {
    response_type: 'average',
    question: 'Were  your  wages for last  month given to  you on  time'
  },
  {
    response_type: 'frequency',
    question: 'During  the last  month,  did you everwork more than  60  hours in  a week',
  },
  {
    response_type: 'average',
    question: 'Were  you fully paid  for the overtime  hours you worked  last  month'
  },
  {
    response_type: 'frequency',
    question: 'Is  your  pay based on  the number  of  items you produce',
  },
  {
    response_type: 'average',
    question: 'Before receiving house rent  and medical allowance,  was your  basic wage  
more  than  2300  taka  last month'
  },
  {
    response_type: 'frequency',
    question: 'how many emergency exits are there',
  },
  {
    response_type: 'average',
    question: 'Are you familiar  with  the factory code  of  conduct'
  },
  {
    response_type: 'frequency',
    question: 'Are the fire  escapes in  your  building  accessible  at  all times',
  },
  {
    response_type: 'record',
    question: 'Please  state the date  ofthe last  fire  safety  training',
  },
  {
    response_type: 'frequency',
    question: 'Have  you experienced any instances of  abuse or  harassment  from  
management  in  the last  month'
  },
  {
    response_type: 'record',
    question: 'Please tell  us  about any dangerous incident or incidents',
  },
  {
    response_type: 'average',
    question: 'Have  any children  worked  in  your  factory in  the last  month'
  },
  {
    response_type: 'frequency',
    question: 'Have  you ever  been  denied  water breaks during your  shift in  the last  month',
  },
  {
    response_type: 'average',
    question: 'Have  you participated  in  fire  safety  training  over  the last  12 months'
  },
  {
    response_type: 'frequency',
    question: 'Is  there anything  else  you want  to  tell  us  about your  working conditions',
  },
  {
    response_type:'average',
    question: 'Is  there firefighting  equipment available in  your  factory at  all times'
  },
  {
    response_type:'record',
    question: 'Describe the how you and the other workers are treated by managment'
  }
]

@welcomes = [
  "Welcome to Labor Voices.  Let me ask you a few questions",  
  "Welcome to Labor Voices.  Thanks for taking the time to speak with us"
]
@goodbyes = [
  "Thats it for today! Thanks for your time.  Goodbye.",  
  "LaborVoices would like to thank you again for all your help.  Goodbye."
]
@groups = [
  "Walmart",
  "Apple"
]
@organizations = [
  "ng-1b",
  "ww-2b",
  "b-3b",
  "ng-1a",
  "qr-1b",
  "f-1c"
]
@docs = [
  "survey-1",
  "survey-2",
  "survey-3",
  "survey-4",
  "survey-5"
]
@reg_docs = [
  "reg-1",
  "reg-2",
  "reg-3",
  "reg-4",
  "reg-5"
]
@letters = [
  "a",
  "b",
  "c",
  "d",
  "e",
  "f",
  "g",
  "j",
  "i",
  "k",
  "l",
  "m",
  "n",
  "o",
  "p",
  "q",
  "r",
  "s",
  "z",
]
@digits = [
  0,1,2,3,4,5,6,7,8,9
]
@truefalse = [
  0,1
]
@response_types = [
  "message",
  "frequency",
  "average",
  "record"
]
@number = "+16235522678"
# methods

def construct_admin
  admin = TwilioSurveyor::Admin.new
  admin.email = "admin@laborvoices.com"
  admin.password = "admin123lv"
  admin.password_confirmation = "admin123lv"
  admin.save
end

def construct_group group_name
  group = TwilioSurveyor::Group.create
  group.reference_name = group_name
  group.summary = @lorem_ipsum.sample
  group.incoming_number = @number
  group.outgoing_number = @number
  group.docs << construct_doc(@reg_docs.sample,@registration_survey,true)
  rand(2..5).times{group.docs << construct_doc(@docs.sample)}
  group.docs << construct_doc(@reg_docs.sample,@greievence_survey,false,true)
  @organizations.each{ |org| group.organizations << construct_org(group_name[0..2].downcase + "_" + org) }
  group.save
end

def construct_doc doc_name, survey_array = nil, registration = false, grievence = false
  doc_name = doc_name + "-" + @letters.sample + @letters.sample
  doc = TwilioSurveyor::Doc.new
  doc.reference_name = grievence ? "grievence-#{rand(0..5)}" : doc_name
  doc.description = @lorem_ipsum.sample
  doc.is_registration = registration
  if survey_array.nil?
    rand(4..8).times{doc.pages << construct_page(@pages.sample,doc.pages.length)}
  else
    survey_array.each do |page|
      doc.pages << construct_page(page,doc.pages.length)
    end
  end
  if (grievence)
    doc.call_filters = {}
    doc.call_filters["grievence"] = "1"
  end
  doc
end

def construct_org organization_name
  organization = TwilioSurveyor::Organization.new
  organization.reference_name = organization_name
  organization.summary = @lorem_ipsum.sample
  organization.code = rand(100..999)
  organization
end

def construct_page page_object, index
  page = TwilioSurveyor::Page.new
  page.reference_name = page_object[:question]
  page.response_type = page_object[:response_type]
  page.index = index
  page.speaks << construct_speak(page.reference_name)
  page.max_length = page_object[:length] || 30
  if page.response_type == "message"
    construct_message page
  elsif page.response_type == "frequency" || page.response_type == "average"
    construct_gather page
  else 
    construct_record page
  end
end

def construct_message page
  page
end

def construct_gather page
  if (rand(0..9) > 6)
    page.tags = {}
    page.tags["control"] = "1"
    page.correct_answer = @digits.sample
  end
  page
end

def construct_record page
  page  
end

def construct_speak say_text
  speak = TwilioSurveyor::Speak.new
  speak.index = 1
  speak.say = TwilioSurveyor::Say.new(value: say_text) 
  speak
end

def rand_phone
  "+1"+ rand(100..999).to_s+"55" + rand(10000..99999).to_s
end

def construct_call doc, user
  call = TwilioSurveyor::Call.new
  call.to = user.phone_number
  call.organization = user.organization
  call.group = user.organization.group unless user.organization.nil?
  call.docs << doc
  doc.pages.each do |page|
    if page.is_gather?
      call.answers << construct_gather_answer(page)
    elsif page.response_type == "record"
      call.answers << construct_record_answer(page)
    end
  end
  call.duration = rand(3..500)
  call.updated_at = Time.now - rand(0..6).months - rand(0..600).minutes
  call.created_at = call.updated_at - call.duration
  call
end

def construct_gather_answer page
  answer = TwilioSurveyor::Answer.new
  if page.has_tag?("control") && (rand(0..9) > 3)
    answer.digits = page.correct_answer
  else
    answer.digits = rand(0..9)
  end
  answer.page = page
  answer.response_duration = rand(0..25)
  answer
end

def construct_record_answer page
  answer = TwilioSurveyor::Answer.new
  answer.page = page
  answer.recording_url = "#{page.doc.id.to_s}/fake_recording-#{((Time.now - rand(0..200).minutes).to_formatted_s(:long_ordinal)).parameterize}.wav"
  answer.recording_duration = rand(5..55)
  answer.response_duration = answer.recording_duration + rand(0..5)
  answer.translation = @lorem_ipsum.sample if (rand(0..9)>4)
  answer
end

# create
puts "*** seeding db ***"

puts "Construct Groups/Orgs/Docs..."
construct_admin
@groups.each do |group_name|
  construct_group(group_name)
end

puts "Register Users..."
TwilioSurveyor::Group.all.each do |group|
  rand(10..15).times do
    user = TwilioSurveyor::User.new
    user.phone_number = rand_phone
    user.groups << group
    user.organization = group.organizations.sample if rand(0..9) < 9
    user.calls << construct_call(group.registration_doc,user)
    user.save
  end
end

puts "Answer Surveys..."
$stdout.sync = true
TwilioSurveyor::User.all.each do |user|
  print "#{user.id}"
  rand(9..12).times do
    call = construct_call(user.current_group.docs.sample,user)
    user.calls << call
    print "."
    TwilioSurveyor::CallStats.update_all_stats(call)
  end
end
$stdout.sync = false

puts "Faking stats created_at..."
TwilioSurveyor::PageStats.all.each{|d| d.created_at = Time.now - rand(0...1800).hours; d.save; }
TwilioSurveyor::DocStats.all.each{|d| d.created_at = Time.now - rand(0...1800).hours; d.save; }
