# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20131003214651) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"
  enable_extension "hstore"

  create_table "twilio_surveyor_admins", force: true do |t|
    t.string   "email",                  default: "", null: false
    t.string   "encrypted_password",     default: "", null: false
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",          default: 0
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string   "current_sign_in_ip"
    t.string   "last_sign_in_ip"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "twilio_surveyor_admins", ["email"], name: "index_twilio_surveyor_admins_on_email", unique: true, using: :btree
  add_index "twilio_surveyor_admins", ["reset_password_token"], name: "index_twilio_surveyor_admins_on_reset_password_token", unique: true, using: :btree

  create_table "twilio_surveyor_alerts", force: true do |t|
    t.integer  "page_id"
    t.boolean  "greater_than"
    t.float    "average"
    t.float    "sigma"
    t.datetime "last_sent"
    t.string   "text_numbers",  array: true
    t.string   "emails",        array: true
    t.string   "text_message"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.text     "email_message"
    t.integer  "group_id"
  end

  add_index "twilio_surveyor_alerts", ["group_id"], name: "index_twilio_surveyor_alerts_on_group_id", using: :btree
  add_index "twilio_surveyor_alerts", ["page_id"], name: "index_twilio_surveyor_alerts_on_page_id", using: :btree

  create_table "twilio_surveyor_answers", force: true do |t|
    t.string   "digits"
    t.string   "recording_url"
    t.integer  "recording_duration"
    t.boolean  "temp_recording",     default: true
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "page_id"
    t.integer  "call_id"
    t.string   "recording_sid"
    t.integer  "response_duration"
    t.text     "translation"
    t.integer  "doc_id"
  end

  add_index "twilio_surveyor_answers", ["call_id"], name: "index_twilio_surveyor_answers_on_call_id", using: :btree
  add_index "twilio_surveyor_answers", ["doc_id"], name: "index_twilio_surveyor_answers_on_doc_id", using: :btree
  add_index "twilio_surveyor_answers", ["page_id"], name: "index_twilio_surveyor_answers_on_page_id", using: :btree

  create_table "twilio_surveyor_call_docs", force: true do |t|
    t.integer  "call_id"
    t.integer  "doc_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "twilio_surveyor_call_docs", ["call_id"], name: "index_twilio_surveyor_call_docs_on_call_id", using: :btree
  add_index "twilio_surveyor_call_docs", ["doc_id"], name: "index_twilio_surveyor_call_docs_on_doc_id", using: :btree

  create_table "twilio_surveyor_call_stats", force: true do |t|
    t.integer  "call_id"
    t.float    "average_difference"
    t.float    "average_sigma_difference"
    t.float    "average_score"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "organization_id"
  end

  add_index "twilio_surveyor_call_stats", ["call_id"], name: "index_twilio_surveyor_call_stats_on_call_id", using: :btree
  add_index "twilio_surveyor_call_stats", ["organization_id"], name: "index_twilio_surveyor_call_stats_on_organization_id", using: :btree

  create_table "twilio_surveyor_calls", force: true do |t|
    t.string   "direction"
    t.string   "to"
    t.string   "from"
    t.string   "duration"
    t.string   "country"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "user_id"
    t.string   "call_sid"
    t.integer  "group_id"
    t.integer  "organization_id"
    t.string   "initiation_type"
  end

  add_index "twilio_surveyor_calls", ["group_id"], name: "index_twilio_surveyor_calls_on_group_id", using: :btree
  add_index "twilio_surveyor_calls", ["organization_id"], name: "index_twilio_surveyor_calls_on_organization_id", using: :btree
  add_index "twilio_surveyor_calls", ["user_id"], name: "index_twilio_surveyor_calls_on_user_id", using: :btree

  create_table "twilio_surveyor_doc_groupings", force: true do |t|
    t.integer  "doc_id"
    t.integer  "group_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "twilio_surveyor_doc_groupings", ["doc_id"], name: "index_twilio_surveyor_doc_groupings_on_doc_id", using: :btree
  add_index "twilio_surveyor_doc_groupings", ["group_id"], name: "index_twilio_surveyor_doc_groupings_on_group_id", using: :btree

  create_table "twilio_surveyor_doc_stats", force: true do |t|
    t.integer  "doc_id"
    t.float    "average_sigma"
    t.float    "average_reliability"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "organization_id"
  end

  add_index "twilio_surveyor_doc_stats", ["doc_id"], name: "index_twilio_surveyor_doc_stats_on_doc_id", using: :btree
  add_index "twilio_surveyor_doc_stats", ["organization_id"], name: "index_twilio_surveyor_doc_stats_on_organization_id", using: :btree

  create_table "twilio_surveyor_docs", force: true do |t|
    t.string   "reference_name"
    t.string   "description"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.hstore   "call_filters"
    t.boolean  "is_registration",    default: false
    t.string   "no_response_action"
    t.integer  "next_doc_id"
  end

  add_index "twilio_surveyor_docs", ["next_doc_id"], name: "index_twilio_surveyor_docs_on_next_doc_id", using: :btree

  create_table "twilio_surveyor_group_stats", force: true do |t|
    t.integer  "group_id"
    t.float    "sigma_score"
    t.float    "reliability_score"
    t.float    "use_score"
    t.float    "rating"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "twilio_surveyor_group_stats", ["group_id"], name: "index_twilio_surveyor_group_stats_on_group_id", using: :btree

  create_table "twilio_surveyor_groups", force: true do |t|
    t.string   "reference_name"
    t.string   "summary"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "incoming_number"
    t.string   "outgoing_number"
    t.string   "outside_number"
  end

  create_table "twilio_surveyor_organization_stats", force: true do |t|
    t.integer  "organization_id"
    t.float    "sigma_score"
    t.float    "reliability_score"
    t.float    "use_score"
    t.float    "rating"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "twilio_surveyor_organization_stats", ["organization_id"], name: "index_twilio_surveyor_organization_stats_on_organization_id", using: :btree

  create_table "twilio_surveyor_organizations", force: true do |t|
    t.integer  "group_id"
    t.string   "reference_name"
    t.string   "summary"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "incoming_number"
    t.string   "outgoing_number"
    t.string   "outside_number"
    t.integer  "code"
  end

  add_index "twilio_surveyor_organizations", ["group_id"], name: "index_twilio_surveyor_organizations_on_group_id", using: :btree

  create_table "twilio_surveyor_page_stats", force: true do |t|
    t.integer  "page_id"
    t.float    "average"
    t.float    "sigma"
    t.float    "reliability"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "organization_id"
  end

  add_index "twilio_surveyor_page_stats", ["organization_id"], name: "index_twilio_surveyor_page_stats_on_organization_id", using: :btree
  add_index "twilio_surveyor_page_stats", ["page_id"], name: "index_twilio_surveyor_page_stats_on_page_id", using: :btree

  create_table "twilio_surveyor_pages", force: true do |t|
    t.integer  "doc_id"
    t.hstore   "menu"
    t.integer  "max_length",      default: 30
    t.string   "response_type",   default: "frequency"
    t.integer  "index"
    t.string   "reference_name"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "response_key"
    t.integer  "num_digits",      default: 1
    t.integer  "correct_answer",  default: -1
    t.hstore   "tags"
    t.integer  "next_page_index"
    t.integer  "timeout",         default: 7
    t.string   "finish_on_key",   default: "#"
    t.boolean  "transcribe",      default: false
    t.string   "valid_entries",                         array: true
  end

  add_index "twilio_surveyor_pages", ["doc_id"], name: "index_twilio_surveyor_pages_on_doc_id", using: :btree

  create_table "twilio_surveyor_plays", force: true do |t|
    t.integer  "speak_id"
    t.integer  "loop",       default: 1
    t.string   "value"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "twilio_surveyor_plays", ["speak_id"], name: "index_twilio_surveyor_plays_on_speak_id", using: :btree

  create_table "twilio_surveyor_registrations", force: true do |t|
    t.integer  "user_id"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "group_id"
  end

  add_index "twilio_surveyor_registrations", ["group_id"], name: "index_twilio_surveyor_registrations_on_group_id", using: :btree
  add_index "twilio_surveyor_registrations", ["user_id"], name: "index_twilio_surveyor_registrations_on_user_id", using: :btree

  create_table "twilio_surveyor_says", force: true do |t|
    t.integer  "speak_id"
    t.string   "voice",      default: "woman"
    t.string   "language",   default: "en"
    t.integer  "loop",       default: 1
    t.text     "value"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "twilio_surveyor_says", ["speak_id"], name: "index_twilio_surveyor_says_on_speak_id", using: :btree

  create_table "twilio_surveyor_silences", force: true do |t|
    t.integer  "page_id"
    t.string   "redirect"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "twilio_surveyor_silences", ["page_id"], name: "index_twilio_surveyor_silences_on_page_id", using: :btree

  create_table "twilio_surveyor_speaks", force: true do |t|
    t.integer  "page_id"
    t.integer  "silence_id"
    t.string   "reference_name"
    t.integer  "index"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "no_response_doc_id"
    t.integer  "end_call_doc_id"
    t.integer  "invalid_entry_doc_id"
    t.integer  "transfer_doc_id"
  end

  add_index "twilio_surveyor_speaks", ["end_call_doc_id"], name: "index_twilio_surveyor_speaks_on_end_call_doc_id", using: :btree
  add_index "twilio_surveyor_speaks", ["invalid_entry_doc_id"], name: "index_twilio_surveyor_speaks_on_invalid_entry_doc_id", using: :btree
  add_index "twilio_surveyor_speaks", ["no_response_doc_id"], name: "index_twilio_surveyor_speaks_on_no_response_doc_id", using: :btree
  add_index "twilio_surveyor_speaks", ["page_id"], name: "index_twilio_surveyor_speaks_on_page_id", using: :btree
  add_index "twilio_surveyor_speaks", ["silence_id"], name: "index_twilio_surveyor_speaks_on_silence_id", using: :btree
  add_index "twilio_surveyor_speaks", ["transfer_doc_id"], name: "index_twilio_surveyor_speaks_on_transfer_doc_id", using: :btree

  create_table "twilio_surveyor_user_stats", force: true do |t|
    t.integer  "user_id"
    t.float    "average_difference"
    t.float    "average_sigma_difference"
    t.float    "average_score"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "twilio_surveyor_user_stats", ["user_id"], name: "index_twilio_surveyor_user_stats_on_user_id", using: :btree

  create_table "twilio_surveyor_users", force: true do |t|
    t.string   "phone_number"
    t.integer  "pin"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "organization_id"
  end

  add_index "twilio_surveyor_users", ["organization_id"], name: "index_twilio_surveyor_users_on_organization_id", using: :btree

end
