Rails.application.routes.draw do

  mount TwilioSurveyorStats::Engine => "/twilio_surveyor", as: 'twilio_surveyor_stats'
  mount TwilioSurveyor::Engine => "/twilio_surveyor", as: 'twilio_surveyor'
end
