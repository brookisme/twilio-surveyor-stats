module TwilioSurveyorStats
  class Engine < ::Rails::Engine
    isolate_namespace TwilioSurveyorStats
  end
end
