TwilioSurveyor::Engine.routes.draw do
  resources :alerts

  controller :doc_stats do
    post "#{TwilioSurveyor::Doc.plural_name}/data/search/#{TwilioSurveyor::Organization.display_name}-stats/" => "doc_stats#index"
    post "#{TwilioSurveyor::Doc.plural_name}/#{TwilioSurveyor::Organization.display_name}-stats/data/search/" => "doc_stats#index"
    post "#{TwilioSurveyor::Doc.plural_name}/:id/#{TwilioSurveyor::Organization.display_name}-stats/"  => "doc_stats#show"
  end
end

TwilioSurveyorStats::Engine.routes.draw do

  # survey routes for override
  controller :survey do
    post :make_call 
    post :call_terminated
    post :recieve_call, defaults: { format: :xml }
  end

  controller :survey,  defaults: { format: :xml } do
    post :twilio_response
  end

end
